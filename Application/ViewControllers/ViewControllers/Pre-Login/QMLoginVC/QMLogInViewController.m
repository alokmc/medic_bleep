//
//  QMLogInViewController.m
//  Medic Bleep
//
//  Created by Igor Alefirenko on 13/02/2014.
//  Copyright (c) 2014 Quickblox. All rights reserved.
//

#import "QMLogInViewController.h"
#import "REAlertView+QMSuccess.h"
#import "QMApi.h"
#import "SVProgressHUD.h"
#import "QMSettingsManager.h"

@interface QMLogInViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UISwitch *rememberMeSwitch;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;

@end

@implementation QMLogInViewController

- (void)dealloc {
    NSLog(@"%@ - %@",  NSStringFromSelector(_cmd), self);
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.logInButton.layer.cornerRadius = 10.0f;
    self.logInButton.layer.masksToBounds = YES;
    
    self.rememberMeSwitch.on = YES;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (IBAction)logIn:(id)sender{
    RETURN_IF_NO_INTERNET_AVAILABLE_WITH_USER_WARNING
    NSString *email = self.emailField.text;
    NSString *password = self.passwordField.text;
    if (email.length == 0 || password.length == 0) {
        [CommonFunctions showToastMessageWithMessage:NSLocalizedString(@"QM_STR_FILL_IN_ALL_THE_FIELDS", nil)];
    }
    else {
        __weak __typeof(self)weakSelf = self;
        [CommonFunctions showActivityIndicatorWithText:nil];
        [[QMApi instance] loginWithEmail:email
                                password:password
                              rememberMe:weakSelf.rememberMeSwitch.on
                              completion:^(BOOL success)
         {
             [CommonFunctions removeActivityIndicator];
             if (success) {
                 [[QMApi instance] setAutoLogin:weakSelf.rememberMeSwitch.on
                                withAccountType:QMAccountTypeEmail];
                 [weakSelf performSegueWithIdentifier:kTabBarSegueIdnetifier
                                               sender:nil];
             }
         }];
    }
}

@end
