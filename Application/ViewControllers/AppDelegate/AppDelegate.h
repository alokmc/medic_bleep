//
//  AppDelegate.h
//  Application
//
//  Created by Alok Kumar Singh on 15/01/16.
//  Copyright © 2016 Aryansbtloe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UINavigationController *navigationController;
@property (strong, nonatomic) id rootViewController;

@end

