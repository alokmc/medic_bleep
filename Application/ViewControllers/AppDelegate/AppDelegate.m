//
//  AppDelegate.m
//  Application
//
//  Created by Alok Kumar Singh on 15/01/16.
//  Copyright © 2016 Aryansbtloe. All rights reserved.
//

#import "AppDelegate.h"
#import "AppCommonFunctions.h"
#import "UIImage+Additions.h"
#import <CoreData/CoreData.h>
#import "SVProgressHUD.h"
#import "REAlertView+QMSuccess.h"
#import "QMApi.h"
#import "QMSettingsManager.h"
#import "QMAVCallManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "QMViewControllersFactory.h"

#define DEVELOPMENT 0
#define STAGE_SERVER_IS_ACTIVE 0

const NSUInteger kQMApplicationID = 31178;
NSString *const kQMAuthorizationKey = @"OUPfpXj9jFnEbbG";
NSString *const kQMAuthorizationSecret = @"SD5-y5wcX4cEFL8";
NSString *const kQMAcconuntKey = @"hNUZvUAb8Q3SGWQ5xYp9";

@implementation AppDelegate

@synthesize navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self initializeGlobalData];
    [self initializeContents];
    [self prepareForApplePushNotificationsService:application];
    [self initializeWindowAndStartUpViewController];
    [self afterInitialisationSetup];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ACF performActionWithNotificationClickWithUserInfo:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
    });
    if (launchOptions != nil) {
        NSDictionary *notification = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
        [[QMApi instance] setPushNotification:notification];
    }
    [self handleOpeningApplicationWithUrl:[launchOptions objectForKey:UIApplicationLaunchOptionsURLKey]];
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)initializeGlobalData {
    fontRegular7  = [UIFont fontWithName:FONT_REGULAR size:7];
    fontRegular8  = [UIFont fontWithName:FONT_REGULAR size:8];
    fontRegular9  = [UIFont fontWithName:FONT_REGULAR size:9];
    fontRegular10 = [UIFont fontWithName:FONT_REGULAR size:10];
    fontRegular11 = [UIFont fontWithName:FONT_REGULAR size:11];
    fontRegular12 = [UIFont fontWithName:FONT_REGULAR size:12];
    fontRegular13 = [UIFont fontWithName:FONT_REGULAR size:13];
    fontRegular14 = [UIFont fontWithName:FONT_REGULAR size:14];
    fontRegular15 = [UIFont fontWithName:FONT_REGULAR size:15];
    fontRegular16 = [UIFont fontWithName:FONT_REGULAR size:16];
    fontRegular17 = [UIFont fontWithName:FONT_REGULAR size:17];
    fontRegular18 = [UIFont fontWithName:FONT_REGULAR size:18];
    fontRegular19 = [UIFont fontWithName:FONT_REGULAR size:19];
    fontRegular20 = [UIFont fontWithName:FONT_REGULAR size:20];
    fontRegular26 = [UIFont fontWithName:FONT_REGULAR size:26];
    fontRegular60 = [UIFont fontWithName:FONT_REGULAR size:60];
    
    fontSemiBold7  = [UIFont fontWithName:FONT_SEMI_BOLD size:7];
    fontSemiBold8  = [UIFont fontWithName:FONT_SEMI_BOLD size:8];
    fontSemiBold9  = [UIFont fontWithName:FONT_SEMI_BOLD size:9];
    fontSemiBold10 = [UIFont fontWithName:FONT_SEMI_BOLD size:10];
    fontSemiBold11 = [UIFont fontWithName:FONT_SEMI_BOLD size:11];
    fontSemiBold12 = [UIFont fontWithName:FONT_SEMI_BOLD size:12];
    fontSemiBold13 = [UIFont fontWithName:FONT_SEMI_BOLD size:13];
    fontSemiBold14 = [UIFont fontWithName:FONT_SEMI_BOLD size:14];
    fontSemiBold15 = [UIFont fontWithName:FONT_SEMI_BOLD size:15];
    fontSemiBold16 = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
    fontSemiBold17 = [UIFont fontWithName:FONT_SEMI_BOLD size:17];
    fontSemiBold18 = [UIFont fontWithName:FONT_SEMI_BOLD size:18];
    fontSemiBold19 = [UIFont fontWithName:FONT_SEMI_BOLD size:19];
    fontSemiBold20 = [UIFont fontWithName:FONT_SEMI_BOLD size:20];
    fontSemiBold26 = [UIFont fontWithName:FONT_SEMI_BOLD size:26];
    fontSemiBold60 = [UIFont fontWithName:FONT_SEMI_BOLD size:60];
    
    fontBold7  = [UIFont fontWithName:FONT_BOLD size:7];
    fontBold8  = [UIFont fontWithName:FONT_BOLD size:8];
    fontBold9  = [UIFont fontWithName:FONT_BOLD size:9];
    fontBold10 = [UIFont fontWithName:FONT_BOLD size:10];
    fontBold11 = [UIFont fontWithName:FONT_BOLD size:11];
    fontBold12 = [UIFont fontWithName:FONT_BOLD size:12];
    fontBold13 = [UIFont fontWithName:FONT_BOLD size:13];
    fontBold14 = [UIFont fontWithName:FONT_BOLD size:14];
    fontBold15 = [UIFont fontWithName:FONT_BOLD size:15];
    fontBold16 = [UIFont fontWithName:FONT_BOLD size:16];
    fontBold17 = [UIFont fontWithName:FONT_BOLD size:17];
    fontBold18 = [UIFont fontWithName:FONT_BOLD size:18];
    fontBold19 = [UIFont fontWithName:FONT_BOLD size:19];
    fontBold20 = [UIFont fontWithName:FONT_BOLD size:20];
    fontBold26 = [UIFont fontWithName:FONT_BOLD size:26];
    fontBold60 = [UIFont fontWithName:FONT_BOLD size:60];
    
    colorType1 = [AKSMethods colorFromHexString:@"1d97e9"];
    colorType2 = [UIColor colorWithWhite:0.922 alpha:1.000];
    colorType3 = [UIColor darkGrayColor];
    colorType4 = [UIColor colorWithWhite:0.500 alpha:0.310];
    
    navigationBarBackgroundImage1 = [UIImage imageWithColor:colorType1 size:CGSizeMake(320, 64)];
}

- (void)initializeContents {
    navigationController = (UINavigationController *)self.window.rootViewController;
    [ACF prepareStartup];
    if ([AKSMethods isApplicationUpdated]) {
        [CommonFunctions clearApplicationCaches];
    }
    if ([self isNull:PUSH_NOTIFICATION_TOKEN] || ((NSString *)PUSH_NOTIFICATION_TOKEN).length == 0) {
        [[NSUserDefaults standardUserDefaults]setObject:FAKE_PUSH_NOTIFICATION_DEVICE_TOKEN forKey:PUSH_NOTIFICATION_DEVICE_TOKEN];
    }
    
    UIApplication.sharedApplication.applicationIconBadgeNumber = 0;
    
    UIApplication.sharedApplication.statusBarStyle = UIStatusBarStyleDefault;
    
    self.window.backgroundColor = [UIColor whiteColor];
    
    // QB Settings
    [QBSettings setApplicationID:kQMApplicationID];
    [QBSettings setAuthKey:kQMAuthorizationKey];
    [QBSettings setAuthSecret:kQMAuthorizationSecret];
    [QBSettings setAccountKey:kQMAcconuntKey];
    
    [QBRTCClient initializeRTC];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
    
#if STAGE_SERVER_IS_ACTIVE == 1
    [QBConnection setApiDomain:@"https://api.stage.quickblox.com" forServiceZone:QBConnectionZoneTypeDevelopment];
    [QBConnection setServiceZone:QBConnectionZoneTypeDevelopment];
    [QBSettings setServerChatDomain:@"chatstage.quickblox.com"];
    [QBSettings setContentBucket: kQMContentBucket];
#endif
    
    /*Configure app appearance*/
    NSDictionary *normalAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithWhite:1.000 alpha:0.750]};
    NSDictionary *disabledAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithWhite:0.935 alpha:0.260]};
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:normalAttributes forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:disabledAttributes forState:UIControlStateDisabled];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UIImagePickerController class], nil] setTitleTextAttributes:nil forState:UIControlStateNormal];
    [[UIBarButtonItem appearanceWhenContainedIn:[UIImagePickerController class], nil] setTitleTextAttributes:nil forState:UIControlStateDisabled];
    

}

#pragma mark - Methods for initialising window and startup view controllers

- (void)initializeWindowAndStartUpViewController {
    [self prepareViews];
    [self.window makeKeyAndVisible];
}

- (void)afterInitialisationSetup {
}

- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame {
    NOTIFY_TO_UPDATE_LAYOUT_CUSTOM
}

- (void)prepareViews {
    HIDE_STATUS_BAR
}

#pragma mark - Local Notifications Delegate Methods

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    [ACF handleLocalRemoteNotification:notification];
}

#pragma mark - Apple Push Notifications Service Methods

- (void)prepareForApplePushNotificationsService:(UIApplication *)application {
    CLEAR_NOTIFICATION_BADGE
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
#ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             | UIRemoteNotificationTypeSound
                                                                                             | UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
#endif
    }
    else {
        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    if ([identifier isEqualToString:@"declineAction"]) {
    }
    else if ([identifier isEqualToString:@"answerAction"]) {
    }
}

#endif

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[NSUserDefaults standardUserDefaults]setObject:[AKSMethods stringWithDeviceToken:deviceToken] forKey:PUSH_NOTIFICATION_DEVICE_TOKEN];
    NSLog(@"Push notification device token %@", [[NSUserDefaults standardUserDefaults]objectForKey:PUSH_NOTIFICATION_DEVICE_TOKEN]);
    
    if (deviceToken) {
        [[QMApi instance] setDeviceToken:deviceToken];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Push notification failed");
    [[NSUserDefaults standardUserDefaults]setObject:FAKE_PUSH_NOTIFICATION_DEVICE_TOKEN forKey:PUSH_NOTIFICATION_DEVICE_TOKEN];
}

#pragma mark - Application Life Cycle Methods

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NOTIFY_TO_UPDATE_LAYOUT_CUSTOM
    if ([AKSMethods isNeedToClearCache]) {
        [CommonFunctions clearApplicationCaches];
    }
    [AKSMethods syncroniseNSUserDefaults];
    [DBM saveData];
    [FBSDKAppEvents activateApp];
}

#pragma mark - Memory management methods

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    [CommonFunctions clearApplicationCaches];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    if ([application applicationState] == UIApplicationStateInactive) {
        NSString *dialogID = userInfo[kPushNotificationDialogIDKey];
        if (dialogID != nil) {
            NSString *dialogWithIDWasEntered = [QMApi instance].settingsManager.dialogWithIDisActive;
            if ([dialogWithIDWasEntered isEqualToString:dialogID]) return;
            [[QMApi instance] setPushNotification:userInfo];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[QMApi instance] handlePushNotificationWithDelegate:self];
            });
        }
        NSLog(@"Push was received. User info: %@", userInfo);
    }
    [ACF handlePushWith:userInfo];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    UIApplication.sharedApplication.applicationIconBadgeNumber = 0;
    [[QMApi instance] applicationWillResignActive];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    if (!QMApi.instance.isInternetConnected) {
        [REAlertView showAlertWithMessage:NSLocalizedString(@"QM_STR_CHECK_INTERNET_CONNECTION", nil) actionSuccess:NO];
        return;
    }
    if (!QMApi.instance.currentUser) {
        return;
    }
    if (![QBChat instance].isConnected) [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [[QMApi instance] applicationDidBecomeActive:^(BOOL success) {}];
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    [self handleOpeningApplicationWithUrl:url.absoluteString];
    BOOL urlWasIntendedForFacebook = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                                    openURL:url
                                                                          sourceApplication:sourceApplication
                                                                                 annotation:annotation];
    return urlWasIntendedForFacebook;
}

#pragma mark - QMNotificationHandlerDelegate protocol

- (void)notificationHandlerDidSucceedFetchingDialog:(QBChatDialog *)chatDialog {
    UITabBarController *rootController = [(UITabBarController *)self.window.rootViewController selectedViewController];
    UINavigationController *navigationController = (UINavigationController *)rootController;
    UIViewController *chatVC = [QMViewControllersFactory chatControllerWithDialog:chatDialog];
    NSString *dialogWithIDWasEntered = [QMApi instance].settingsManager.dialogWithIDisActive;
    if (dialogWithIDWasEntered != nil) {
        [navigationController popViewControllerAnimated:NO];
    }
    [navigationController pushViewController:chatVC animated:YES];
}

- (void)handleOpeningApplicationWithUrl:(NSString*)url{
    if ([url containsString:@"mediccreations.medicbleep"]){
        NSURLComponents * urlComponents = [[NSURLComponents alloc]initWithString:url];
        NSArray *queryItems = urlComponents .queryItems;
        NSString *email = [self valueForKey:@"email" fromQueryItems:queryItems];
        if (email != nil){
            [self decideUserTryingToLoginWith:email];
        }
    }
}

- (NSString *)valueForKey:(NSString *)key
           fromQueryItems:(NSArray *)queryItems{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems
                                  filteredArrayUsingPredicate:predicate]
                                 firstObject];
    return queryItem.value;
}

- (void)decideUserTryingToLoginWith:(NSString*)emailId{
    if ([self.currentUser.email isEqualToString:emailId]){
        //everything is okay as user is already logged in
        //everything is already done,no need to do anything
    }else{
        //either user is not logged in or logged in with different account
        if (self.currentUser.email.length > 0){//lets check
            //another user is logged in , lets do logout first
            [SVProgressHUD  showWithMaskType:SVProgressHUDMaskTypeClear];
            [[QMApi instance] logoutWithCompletion:^(BOOL success) {
                [[QMApi instance]loginWithEmail:emailId password:emailId rememberMe:TRUE completion:^(BOOL success) {
                    [SVProgressHUD dismiss];
                    if (success){
                        
                    }else{
                        
                    }
                }];
            }];
        }
    }
}

- (void)resetUIAndShowLoginScreen{
}

- (void)resetUIAndShowHomeScreen{
}

@end
