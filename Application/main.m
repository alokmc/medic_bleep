//
//  main.m
//  Application
//
//  Created by Alok Kumar Singh on 15/01/16.
//  Copyright © 2016 Aryansbtloe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
