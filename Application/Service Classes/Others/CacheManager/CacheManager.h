//
//  Last Updated by Alok on 14/08/14.
//  Copyright (c) 2014 CoreAura. All rights reserved.
//


#import <Foundation/Foundation.h>

#define CACHE_KEY_APP_TUITORIAL_VIDEO_FIRST_FRAME @"CACHE_KEY_APP_TUITORIAL_VIDEO_FIRST_FRAME"

@interface CacheManager : NSObject

+ (CacheManager *)sharedInstance;
- (id)getCachedDataForKey:(NSString *)key;
- (void)setCachedData:(id)data ForKey:(NSString *)key onDisk:(BOOL)onDisk;
- (void)removeObjectForKey:(NSString *)key;
- (void)clearAllData;

@end
