//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface SimpleLabelCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *info;
@property (nonatomic, strong) IBOutlet UIImageView *arrow;

+ (float)getRequiredHeight;

@end
