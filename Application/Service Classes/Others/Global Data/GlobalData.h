//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern UIFont *fontRegular7;
extern UIFont *fontRegular8;
extern UIFont *fontRegular9;
extern UIFont *fontRegular10;
extern UIFont *fontRegular11;
extern UIFont *fontRegular12;
extern UIFont *fontRegular13;
extern UIFont *fontRegular14;
extern UIFont *fontRegular15;
extern UIFont *fontRegular16;
extern UIFont *fontRegular17;
extern UIFont *fontRegular18;
extern UIFont *fontRegular19;
extern UIFont *fontRegular20;
extern UIFont *fontRegular26;
extern UIFont *fontRegular60;

extern UIFont *fontSemiBold7;
extern UIFont *fontSemiBold8;
extern UIFont *fontSemiBold9;
extern UIFont *fontSemiBold10;
extern UIFont *fontSemiBold11;
extern UIFont *fontSemiBold12;
extern UIFont *fontSemiBold13;
extern UIFont *fontSemiBold14;
extern UIFont *fontSemiBold15;
extern UIFont *fontSemiBold16;
extern UIFont *fontSemiBold17;
extern UIFont *fontSemiBold18;
extern UIFont *fontSemiBold19;
extern UIFont *fontSemiBold20;
extern UIFont *fontSemiBold26;
extern UIFont *fontSemiBold60;

extern UIFont *fontBold7;
extern UIFont *fontBold8;
extern UIFont *fontBold9;
extern UIFont *fontBold10;
extern UIFont *fontBold11;
extern UIFont *fontBold12;
extern UIFont *fontBold13;
extern UIFont *fontBold14;
extern UIFont *fontBold15;
extern UIFont *fontBold16;
extern UIFont *fontBold17;
extern UIFont *fontBold18;
extern UIFont *fontBold19;
extern UIFont *fontBold20;
extern UIFont *fontBold26;
extern UIFont *fontBold60;

extern UIImage *navigationBarBackgroundImage1;

extern UIColor *colorType1;
extern UIColor *colorType2;
extern UIColor *colorType3;
extern UIColor *colorType4;

