//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#import "GlobalData.h"

UIFont *fontRegular7 = nil;
UIFont *fontRegular8 = nil;
UIFont *fontRegular9 = nil;
UIFont *fontRegular10 = nil;
UIFont *fontRegular11 = nil;
UIFont *fontRegular12 = nil;
UIFont *fontRegular13 = nil;
UIFont *fontRegular14 = nil;
UIFont *fontRegular15 = nil;
UIFont *fontRegular16 = nil;
UIFont *fontRegular17 = nil;
UIFont *fontRegular18 = nil;
UIFont *fontRegular19 = nil;
UIFont *fontRegular20 = nil;
UIFont *fontRegular26 = nil;
UIFont *fontRegular60 = nil;


UIFont *fontSemiBold7 = nil;
UIFont *fontSemiBold8 = nil;
UIFont *fontSemiBold9 = nil;
UIFont *fontSemiBold10 = nil;
UIFont *fontSemiBold11 = nil;
UIFont *fontSemiBold12 = nil;
UIFont *fontSemiBold13 = nil;
UIFont *fontSemiBold14 = nil;
UIFont *fontSemiBold15 = nil;
UIFont *fontSemiBold16 = nil;
UIFont *fontSemiBold17 = nil;
UIFont *fontSemiBold18 = nil;
UIFont *fontSemiBold19 = nil;
UIFont *fontSemiBold20 = nil;
UIFont *fontSemiBold26 = nil;
UIFont *fontSemiBold60 = nil;

UIFont *fontBold7 = nil;
UIFont *fontBold8 = nil;
UIFont *fontBold9 = nil;
UIFont *fontBold10 = nil;
UIFont *fontBold11 = nil;
UIFont *fontBold12 = nil;
UIFont *fontBold13 = nil;
UIFont *fontBold14 = nil;
UIFont *fontBold15 = nil;
UIFont *fontBold16 = nil;
UIFont *fontBold17 = nil;
UIFont *fontBold18 = nil;
UIFont *fontBold19 = nil;
UIFont *fontBold20 = nil;
UIFont *fontBold26 = nil;
UIFont *fontBold60 = nil;

UIImage *navigationBarBackgroundImage1 = nil;

UIColor *colorType1 = nil;
UIColor *colorType2 = nil;
UIColor *colorType3 = nil;
UIColor *colorType4 = nil;

