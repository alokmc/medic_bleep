//
//  Last Updated by Alok on 14/08/14.
//  Copyright (c) 2014 CoreAura. All rights reserved.
//


#import "FeedItemServices.h"
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "Reachability.h"


@implementation FeedItemServices

@synthesize responseErrorOption;
@synthesize progresssIndicatorText;
@synthesize returnFailureResponseAlso;

/**
 METHODS TO DIRECTLY GET UPDATED DATA FROM THE SERVER
 */

#define RESPONSE_STATUS_KEY  @"error_code"
#define RESPONSE_MESSAGE_KEY @"response_string"

- (id)init {
    self = [super init];
    if (self) {
        [self setResponseErrorOption:ShowErrorResponseWithUsingNotification];
    }
    return self;
}

#define CONTINUE_IF_CONNECTION_AVAILABLE_SHOW_ERROR_MSG if (![self getStatusForNetworkConnectionAndShowUnavailabilityMessage:YES]) { operationFinishedBlock(nil); return; }
#define CONTINUE_IF_CONNECTION_AVAILABLE                if (![self getStatusForNetworkConnectionAndShowUnavailabilityMessage:NO]) { operationFinishedBlock(nil); return; }
#define CONTINUE_IF_CONNECTION_AVAILABLE_1              if (![self getStatusForNetworkConnectionAndShowUnavailabilityMessage:NO]) { operationFinishedBlock(); return; }

- (void)addCommonInformationInto:(NSMutableDictionary *)bodyData {
}

- (void)login:(NSMutableDictionary *)info didFinished:(operationFinishedBlock)operationFinishedBlock {
    NSMutableDictionary *bodyData = [[NSMutableDictionary alloc]init];
    [AKSMethods From:info WithKey:@"email"                To:bodyData U:@"email"  OnM:M_N];
    [AKSMethods From:info WithKey:@"password"             To:bodyData U:@"password"  OnM:M_N];
    if ([self isNull:[info objectForKey:@"password"]]) {
        [AKSMethods addP:@"1" To:bodyData U:@"is_login_with_fb"                          OnM:M_N];
    }
    else {
        [AKSMethods addP:@"0" To:bodyData U:@"is_login_with_fb"                          OnM:M_N];
    }
    [self addCommonInformationInto:bodyData];
    [self performPostRequestWithJsonBody:bodyData toUrl:[NSString stringWithFormat:@"%@/%@", BASE_URL, LOGIN] withFinishedBlock:operationFinishedBlock];
}

- (void)performPostRequestWithJsonBody:(NSMutableDictionary *)bodyData toUrl:(NSString *)url withFinishedBlock:(operationFinishedBlock)operationFinishedBlock {
    CONTINUE_IF_CONNECTION_AVAILABLE_SHOW_ERROR_MSG
    NSString *urlToHit = [url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
#if PRINT_WEB_SERVICES_DETAILS
    NSLog(@"\n\n\n                  HITTING URL\n\n %@\n\n\n                  WITH POST JSON BODY\n\n%@\n\n", urlToHit, bodyData);
#endif
    if (progresssIndicatorText != nil)
        [CommonFunctions showActivityIndicatorWithText:progresssIndicatorText];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager POST:urlToHit parameters:bodyData success:^(NSURLSessionDataTask *task, id responseObject) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:responseObject IfError:nil];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:nil IfError:error];
    }];
}

- (void)performPostRequestWithBody:(NSMutableDictionary *)bodyData toUrl:(NSString *)url withFinishedBlock:(operationFinishedBlock)operationFinishedBlock {
    CONTINUE_IF_CONNECTION_AVAILABLE_SHOW_ERROR_MSG
    NSString *urlToHit = [url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
#if PRINT_WEB_SERVICES_DETAILS
    NSLog(@"\n\n\n                  HITTING URL\n\n %@\n\n\n                  WITH POST BODY\n\n%@\n\n", urlToHit, bodyData);
#endif
    if (progresssIndicatorText != nil)
        [CommonFunctions showActivityIndicatorWithText:progresssIndicatorText];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [manager POST:urlToHit parameters:bodyData success:^(NSURLSessionDataTask *task, id responseObject) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:responseObject IfError:nil];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:nil IfError:error];
    }];

}

- (void)performPostRequestWithBody:(NSMutableDictionary *)bodyData toUrl:(NSString *)url constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block withFinishedBlock:(operationFinishedBlock)operationFinishedBlock {
    CONTINUE_IF_CONNECTION_AVAILABLE_SHOW_ERROR_MSG
    NSString *urlToHit = [url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
#if PRINT_WEB_SERVICES_DETAILS
    NSLog(@"\n\n\n                  HITTING URL\n\n %@\n\n\n                  WITH POST BODY\n\n%@\n\n", urlToHit, bodyData);
#endif
    if (progresssIndicatorText != nil)
        [CommonFunctions showActivityIndicatorWithText:progresssIndicatorText];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [manager POST:urlToHit parameters:bodyData constructingBodyWithBlock:block success:^(NSURLSessionDataTask *task, id responseObject) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:responseObject IfError:nil];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:nil IfError:error];
    }];
}

- (void)performGetRequestWithParameters:(NSMutableDictionary *)parameters toUrl:(NSString *)url withFinishedBlock:(operationFinishedBlock)operationFinishedBlock {
    CONTINUE_IF_CONNECTION_AVAILABLE_SHOW_ERROR_MSG
    NSString *urlToHit = [url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
#if PRINT_WEB_SERVICES_DETAILS
    NSLog(@"\n\n\n                  HITTING URL\n\n %@\n\n\n                  WITH GET PARAMETERS\n\n%@\n\n", urlToHit, parameters);
#endif
    if (progresssIndicatorText != nil)
        [CommonFunctions showActivityIndicatorWithText:progresssIndicatorText];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [manager GET:urlToHit parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:responseObject IfError:nil];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self verifyServerResponseAndPerformAction:operationFinishedBlock WithResponseData:nil IfError:error];
    }];
}

- (void)verifyServerResponseAndPerformAction:(operationFinishedBlock)block WithResponseData:(id)responseData IfError:(NSError *)error {
    BOOL removeActivityIndicator = (progresssIndicatorText != nil);
    if (removeActivityIndicator)
        [CommonFunctions removeActivityIndicator];
    
    if ([self isNotNull:error]) {
        if (responseErrorOption != DontShowErrorResponseMessage) {
            [self showServerNotRespondingMessage];
        }
        [FeedItemServices printErrorMessage:error];
        block(nil);
    }
    else if ([self isNotNull:responseData]) {
        id responseDictionary = [self getParsedDataFrom:responseData];
        if ([responseDictionary isKindOfClass:[NSDictionary class]]) {
            if ([self isSuccess:responseDictionary]) {
                block(responseDictionary);
            }
            else if ([self isFailure:responseDictionary]) {
                NSString *errorMessage = nil;
                if ([self isNotNull:[responseDictionary objectForKey:RESPONSE_MESSAGE_KEY]]) {
                    if ([[responseDictionary objectForKey:RESPONSE_MESSAGE_KEY]isKindOfClass:[NSString class]]) {
                        errorMessage = [responseDictionary objectForKey:RESPONSE_MESSAGE_KEY];
                    }
                    else {
                        NSMutableDictionary *dictionaryObject = [responseDictionary objectForKey:RESPONSE_MESSAGE_KEY];
                        while (([dictionaryObject isKindOfClass:[NSDictionary class]]) && ([[dictionaryObject allKeys]count] > 0)) {
                            errorMessage = [dictionaryObject objectForKey:[[dictionaryObject allKeys]objectAtIndex:0]];
                            dictionaryObject = (NSMutableDictionary *)errorMessage;
                        }
                        if ([self isNull:errorMessage] || (![errorMessage isKindOfClass:[NSString class]])) {
                            errorMessage = MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY;
                        }
                    }
                }
                else {
                    errorMessage = MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY;
                }
                
                if (responseErrorOption == ShowErrorResponseWithUsingNotification) {
                    [CommonFunctions showNotificationInViewController:APPDELEGATE.window.rootViewController withTitle:nil withMessage:errorMessage withType:TSMessageNotificationTypeError withDuration:MIN_DUR];
                }
                else if (responseErrorOption == ShowErrorResponseWithUsingPopUp) {
                    [CommonFunctions showMessageWithTitle:@"Error" withMessage:errorMessage];
                }
                block(returnFailureResponseAlso ? responseDictionary : nil);
            }
            else {
                if (responseErrorOption == ShowErrorResponseWithUsingNotification) {
                    [self showServerNotRespondingMessage];
                }
                block(nil);
            }
        }
        else {
            if (responseErrorOption == ShowErrorResponseWithUsingNotification) {
                [self showServerNotRespondingMessage];
            }
            block(nil);
        }
    }
    else {
        block(nil);
    }
}

#pragma mark - common method for Internet reachability checking

- (BOOL)getStatusForNetworkConnectionAndShowUnavailabilityMessage:(BOOL)showMessage {
    if (([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)) {
        if (showMessage == NO) return NO;
        [JDStatusBarNotification setDefaultStyle: ^JDStatusBarStyle *(JDStatusBarStyle *style) {
            style.barColor = [UIColor colorWithRed:0.797 green:0.148 blue:0.227 alpha:1.000];
            style.textColor = [UIColor whiteColor];
            style.animationType = JDStatusBarAnimationTypeMove;
            style.font = fontRegular12;
            return style;
        }];
        [JDStatusBarNotification showWithStatus:MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY dismissAfter:MIN_DUR];
        return NO;
    }
    return YES;
}

- (void)showServerNotRespondingMessage {
    [CommonFunctions showNotificationInViewController:APPDELEGATE.window.rootViewController withTitle:MESSAGE_TITLE___FOR_SERVER_NOT_REACHABILITY withMessage:MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY withType:TSMessageNotificationTypeError withDuration:MIN_DUR];
}

#pragma mark - common method parse and return the data

- (id)getParsedDataFrom:(NSData *)dataReceived {
    NSString *dataAsString = [[NSString alloc] initWithData:dataReceived encoding:NSUTF8StringEncoding];
    id parsedData   = [NSJSONSerialization JSONObjectWithData:[dataAsString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
#if PRINT_WEB_SERVICES_DETAILS
    NSLog(@"\n\nRECEIVED DATA BEFORE PARSING IS \n\n%@\n\n\n", dataAsString);
    NSLog(@"\n\nRECEIVED DATA AFTER PARSING IS \n\n%@\n\n\n", parsedData);
#endif
    return parsedData;
}

- (NSString *)stringByStrippingHTMLFromString:(NSString *)string {
    NSRange r;
    NSString *s = string;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (void)prepareRequestForGetMethod:(NSMutableURLRequest *)request {
    [self addCredentialsToRequest:request];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
}

- (void)reportMissingParameterWithName:(NSString *)missingParameter WhileRequestingWithMethodName:(NSString *)method {
#if PRINT_MISSING_PARAMETER_DETAILS
    NSString *report = [NSString stringWithFormat:@"\nMISSING PARAMETER :--- %@ ---IN METHOD : %@\n", missingParameter, method];
    NSLog(@"%@", report);
#endif
}

- (BOOL)isSuccess:(NSMutableDictionary *)response {
    if ([response isKindOfClass:[NSDictionary class]]) {
        NSString *requestStatus = [NSString stringWithFormat:@"%@", [response objectForKey:RESPONSE_STATUS_KEY]];
        if ([requestStatus isEqualToString:@"0"]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isFailure:(NSMutableDictionary *)response {
    if ([response isKindOfClass:[NSDictionary class]]) {
        NSString *requestStatus = [NSString stringWithFormat:@"%@", [response objectForKey:RESPONSE_STATUS_KEY]];
        if ([self isNotNull:requestStatus] && (![requestStatus isEqualToString:@"0"])) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - common method to add credentials to request

- (void)addCredentialsToRequest:(NSMutableURLRequest *)request {
#define NEED_TO_ADD_CREDENTIALS FALSE
    if (NEED_TO_ADD_CREDENTIALS) {
        NSString *userName = @"";
        NSString *password = @"";
        if ([self isNotNull:userName] && [self isNotNull:password]) {
            [request addValue:[@"Basic "stringByAppendingFormat:@"%@", [self encode:[[NSString stringWithFormat:@"%@:%@", userName, password] dataUsingEncoding:NSUTF8StringEncoding]]] forHTTPHeaderField:@"Authorization"];
        }
    }
}

#pragma mark - common method to do some encoding

static char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
- (NSString *)encode:(NSData *)plainText {
    int encodedLength = (4 * (([plainText length] / 3) + (1 - (3 - ([plainText length] % 3)) / 3))) + 1;
    unsigned char *outputBuffer = malloc(encodedLength);
    unsigned char *inputBuffer = (unsigned char *)[plainText bytes];
    NSInteger i;
    NSInteger j = 0;
    int remain;
    for (i = 0; i < [plainText length]; i += 3) {
        remain = [plainText length] - i;
        outputBuffer[j++] = alphabet[(inputBuffer[i] & 0xFC) >> 2];
        outputBuffer[j++] = alphabet[((inputBuffer[i] & 0x03) << 4) |
                                     ((remain > 1) ? ((inputBuffer[i + 1] & 0xF0) >> 4) : 0)];
        
        if (remain > 1)
            outputBuffer[j++] = alphabet[((inputBuffer[i + 1] & 0x0F) << 2)
                                         | ((remain > 2) ? ((inputBuffer[i + 2] & 0xC0) >> 6) : 0)];
        else outputBuffer[j++] = '=';
        
        if (remain > 2) outputBuffer[j++] = alphabet[inputBuffer[i + 2] & 0x3F];
        else outputBuffer[j++] = '=';
    }
    outputBuffer[j] = 0;
    NSString *result = [NSString stringWithCString:outputBuffer length:strlen(outputBuffer)];
    free(outputBuffer);
    return result;
}

+ (void)printErrorMessage:(NSError *)error {
    if (error) {
        NSLog(@"[error localizedDescription]        : %@", [error localizedDescription]);
        NSLog(@"[error localizedFailureReason]      : %@", [error localizedFailureReason]);
        NSLog(@"[error localizedRecoverySuggestion] : %@", [error localizedRecoverySuggestion]);
    }
}

@end
