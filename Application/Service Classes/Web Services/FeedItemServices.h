//
//  Last Updated by Alok on 14/08/14.
//  Copyright (c) 2014 CoreAura. All rights reserved.
//


/**
 FeedItemServices:-
 This service class initiates and handles all server interaction related network connection.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JDStatusBarNotification.h"

typedef NS_ENUM (NSInteger, ResponseErrorOption) {
    DontShowErrorResponseMessage = 0,
    ShowErrorResponseWithUsingNotification, //Default value is set to this option
    ShowErrorResponseWithUsingPopUp
};

@class AppDelegate;

typedef void (^operationFinishedBlock)(id responseData);

@interface FeedItemServices : NSObject

@property (nonatomic, readwrite) ResponseErrorOption responseErrorOption;
@property (nonatomic, strong) NSString *progresssIndicatorText;
@property (nonatomic, readwrite) BOOL returnFailureResponseAlso;

- (void)login:(NSMutableDictionary *)info didFinished:(operationFinishedBlock)operationFinishedBlock;

@end

#define  BASE_URL                               @"http://trackrecord.com"
#define  LOGIN                                  @"api/auth/login"
