//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <UIKit/UIKit.h>

@interface DatabaseManager : NSObject

+ (DatabaseManager *)sharedDatabaseManager;

- (NSMutableArray *)getAllContentsFor:(Class)entity;
- (void)resetCompleteDatabase;
- (void)saveData;
- (void)deleteObject:(id)object;

- (int)getUniqueClientId;
- (NSString *)getUniqueIdentifier;

@end
