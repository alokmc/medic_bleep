//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//


#import "DatabaseManager.h"
#import "DKPredicateBuilder.h"
#import "CoreDataModals.h"
#import "MagicalRecord.h"
#import "NSObject+PE.h"

@implementation DatabaseManager

#define MANAGED_OBJECT_CONTEXT [NSManagedObjectContext MR_defaultContext]

static NSString * const storeName = @"Application.sqlite";

#pragma mark - initializer methods

static DatabaseManager *databaseManager_ = nil;

+ (DatabaseManager *)sharedDatabaseManager {
    @synchronized(databaseManager_) {
        static dispatch_once_t pred;
        dispatch_once(&pred, ^{
            if (databaseManager_ == nil) {
                databaseManager_ = [[DatabaseManager alloc]init];
                [MagicalRecord setupCoreDataStackWithStoreNamed:storeName];
                [databaseManager_ copyDefaultStoreIfNecessary];
                MANAGED_OBJECT_CONTEXT;
            }
        });
    }
    
    return databaseManager_;
}

- (void)copyDefaultStoreIfNecessary; {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *storeURL = [NSPersistentStore MR_urlForStoreName:storeName];
    if (![fileManager fileExistsAtPath:[storeURL path]]) {
        NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:[storeName stringByDeletingPathExtension] ofType:[storeName pathExtension]];
        if (defaultStorePath) {
            NSError *error;
            BOOL success = [fileManager copyItemAtPath:defaultStorePath toPath:[storeURL path] error:&error];
            if (!success) {
                NSLog(@"Failed to install default store");
            }
        }
    }
}

+ (id)alloc {
    NSAssert(databaseManager_ == nil, @"Attempted to allocate a second instance of a singleton.");
    return [super alloc];
}


#pragma mark - commons to work with core data

- (void)saveData {
    [MANAGED_OBJECT_CONTEXT processPendingChanges];
    [MANAGED_OBJECT_CONTEXT MR_saveToPersistentStoreAndWait];
    [MANAGED_OBJECT_CONTEXT save:nil];
}

- (void)deleteObject:(id)object {
    [MANAGED_OBJECT_CONTEXT deleteObject:object];
    [self saveData];
    NSError *error = nil;
    if ([self isNotNull:error]) {
        [AKSMethods showDebuggingMessage:error.description];
    }
}

- (NSPredicate *)preparePredicateByAndingKeysAndObjectFromDictionary:(NSDictionary *)info {
    DKPredicateBuilder *predicateBuilder = [[DKPredicateBuilder alloc] init];
    for (NSString *key in info.allKeys) {
        if ([self isNotNull:[info objectForKey:key]])
            [predicateBuilder where:key contains:[info objectForKey:key]];
    }
    return [NSPredicate predicateWithFormat:[[predicateBuilder compoundPredicate] predicateFormat]];
}

- (void)resetCompleteDatabase {
    [self saveData];
}

- (NSMutableArray *)getAllContentsFor:(Class)entity {
    return [self getAllContentsUsingContext:MANAGED_OBJECT_CONTEXT ForEntity:entity WithPredicate:nil asDictionary:NO];
}

- (NSMutableArray *)getAllContentsUsingContext:(NSManagedObjectContext *)context ForEntity:(Class)entity WithPredicate:(NSPredicate *)predicate asDictionary:(BOOL)asDictionary {
    if (predicate) {
        NSLog(@"\n\n\n Class %@ \n predicate applied %@\n\n\n", [entity description], [predicate predicateFormat]);
    }
    NSArray *results = [entity MR_findAllWithPredicate:predicate];
    return [[NSMutableArray alloc]initWithArray:results];
}

- (id)fetchSingleForEntity:(Class)entity WithPredicate:(NSPredicate *)predicate {
    return [entity MR_findFirstWithPredicate:predicate];
}

- (int)getUniqueClientId {
    NSNumber *uniqueId = [[NSUserDefaults standardUserDefaults]objectForKey:@"uniqueId"];
    if (uniqueId == nil)
        uniqueId = [NSNumber numberWithInt:0];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:(uniqueId.integerValue + 1)] forKey:@"uniqueId"];
    return (uniqueId.intValue + 1);
}

- (NSString *)getUniqueIdentifier {
    return [NSString stringWithFormat:@"%d", [self getUniqueClientId]];
}

@end
