//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#import "AKSSocialSharingHelper.h"
#import "KVNProgress.h"

id aKSSocialSharingHelperTempStorage = nil;

static AKSSocialSharingHelper *aKSSocialSharingHelper_ = nil;

@implementation AKSSocialSharingHelper

+ (AKSSocialSharingHelper *)sharedAKSSocialSharingHelper {
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        if (aKSSocialSharingHelper_ == nil) {
            aKSSocialSharingHelper_ = [[AKSSocialSharingHelper alloc]init];
        }
    });
    return aKSSocialSharingHelper_;
}

+ (id)alloc {
    NSAssert(aKSSocialSharingHelper_ == nil, @"Attempted to allocate a second instance of a singleton.");
    return [super alloc];
}

#define MAX_RECEPIENTS_ALLOWED 120

- (void)smsInfo:(NSMutableDictionary *)info fromViewController:(UIViewController *)viewController {
    NSString *bodyOfMessage = [info objectForKey:@"message"];
    NSArray *recipients = [info objectForKey:@"recipients"];
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    [controller setMessageComposeDelegate:self];
    if ([MFMessageComposeViewController canSendText]) {
        controller.body = bodyOfMessage;
        controller.recipients = [recipients count] > MAX_RECEPIENTS_ALLOWED ? [recipients subarrayWithRange:NSMakeRange(0, MAX_RECEPIENTS_ALLOWED)] : recipients;
        [viewController presentModalViewController:controller animated:YES];
    }
    aKSSocialSharingHelperTempStorage = viewController;
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    if (result == MessageComposeResultFailed) {
        [AKSMethods showMessage:@"Failed to send message. Please try again."];
    }
    [((UIViewController *)aKSSocialSharingHelperTempStorage).parentViewController dismissViewControllerAnimated:YES completion: ^{
        aKSSocialSharingHelperTempStorage = nil;
    }];
}

#pragma mark - Sharing Via Email Related Methods

- (void)emailInfo:(NSMutableDictionary *)info fromViewController:(UIViewController *)viewController {
    if (![MFMailComposeViewController canSendMail]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Email Configuration"
                              message:@"We cannot send an email right now because your device's email account is not configured. Please configure an email account from your device's Settings, and try again."
		                            delegate:nil
                              cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    MFMailComposeViewController *emailer = [[MFMailComposeViewController alloc] init];
    emailer.mailComposeDelegate = self;
    
    NSArray *recepients = [info objectForKey:@"recipients"];
    NSString *subject = [info objectForKey:@"subject"];
    NSString *matterText = [info objectForKey:@"message"];
    
    if ([self isNotNull:subject]) [emailer setSubject:subject];
    
    if ([self isNotNull:matterText]) [emailer setMessageBody:matterText isHTML:NO];
    
    NSMutableArray *attachments = [info objectForKey:@"attachments"];
    
    if ([self isNotNull:attachments]) {
        for (int i = 0; i < attachments.count; i++) {
            NSMutableDictionary *attachment = [attachments objectAtIndex:i];
            
            NSData *attachmentData = [attachment objectForKey:@"attachmentData"];
            NSString *attachmentFileName = [attachment objectForKey:@"attachmentFileName"];
            NSString *attachmentFileMimeType = [attachment objectForKey:@"attachmentFileMimeType"];
            
            [emailer addAttachmentData:attachmentData mimeType:attachmentFileMimeType fileName:attachmentFileName];
        }
    }
    
    [emailer setToRecipients:recepients];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        emailer.modalPresentationStyle = UIModalPresentationPageSheet;
    }
    
    [viewController presentViewController:emailer animated:YES completion:nil];
    
    aKSSocialSharingHelperTempStorage = viewController;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (result == MFMailComposeResultFailed) {
        [AKSMethods showMessage:@"Failed to send email. Please try again."];
    }
    [((UIViewController *)aKSSocialSharingHelperTempStorage).parentViewController dismissViewControllerAnimated:YES completion: ^{
        aKSSocialSharingHelperTempStorage = nil;
    }];
}

@end
