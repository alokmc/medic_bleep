//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

@interface AKSSocialSharingHelper : NSObject

+ (AKSSocialSharingHelper *)sharedAKSSocialSharingHelper;
- (void)smsInfo:(NSMutableDictionary *)info fromViewController:(UIViewController *)viewController;
- (void)emailInfo:(NSMutableDictionary *)info fromViewController:(UIViewController *)viewController;

@end
