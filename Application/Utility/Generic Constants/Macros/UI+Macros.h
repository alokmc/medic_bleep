//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//


#ifndef ViewController_Macros_h
#define ViewController_Macros_h

#define RETURN_IF_THIS_VIEW_IS_NOT_A_TOPVIEW_CONTROLLER if (self.navigationController) if (!(self.navigationController.topViewController == self)) return;

#define SHOW_STATUS_BAR               [CommonFunctions showStausBar:YES];
#define HIDE_STATUS_BAR               [CommonFunctions showStausBar:NO];

#define SHOW_NAVIGATION_BAR           [self.navigationController setNavigationBarHidden:NO animated:NO]; self.navigationController.navigationBar.translucent = true;

#define HIDE_NAVIGATION_BAR           [self.navigationController setNavigationBarHidden:YES animated:NO];
#define RESIGN_KEYBOARD               [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
#define CLEAR_NOTIFICATION_BADGE      [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

#define FONT_BOLD                  @"MavenProBold"
#define FONT_SEMI_BOLD             @"MavenProMedium"
#define FONT_REGULAR               @"MavenProLight200-Regular"

#define APPDELEGATE                                     ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define HIDE_NETWORK_ACTIVITY_INDICATOR                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
#define SHOW_NETWORK_ACTIVITY_INDICATOR                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

#define SCREEN_FRAME_RECT                               [[UIScreen mainScreen] bounds]
#define DEVICE_WIDTH                                    [UIScreen mainScreen].bounds.size.width
#define DEVICE_HEIGHT                                   [UIScreen mainScreen].bounds.size.height
#define VIEW_WIDTH                                      [[self view]frame].size.width
#define VIEW_HEIGHT                                     [[self view]frame].size.height

#define DATE_FORMAT_USED @"yyyy'-'MM'-'dd' 'HH':'mm':'ss"

#define NAVIGATION_BAR_HEIGHT 44

#endif
