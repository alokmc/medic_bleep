//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#ifndef ApplicationCommonHeaderIncludes_h
#define ApplicationCommonHeaderIncludes_h

/**
 Import headers of frequently used classes
 Note:- Do not import unnecessary files as them may slow down application compile time as well as can increase the application build size.
 */

/**
 Default frequently used
 */
#import <QuartzCore/QuartzCore.h>


/**
 Commonly used frameworks
 */
#import "AKSMethods.h"
#import "AksAnimations.h"
#import "CommonFunctions.h"
#import "ApplicationSpecificConstants.h"
#import "GlobalData.h"
#import "FCFileManager.h"
#import "AllCategories.h"
#import "All Macros.h"
#import "AppDelegate.h"
#import "FeedItemServices.h"
#import <objc/runtime.h>
#import "AppCommonFunctions.h"
#import "CoreDataModals.h"
#import "DatabaseManager.h"
#import "UIActionSheet+Blocks.h"
#import <CoreData/CoreData.h>
#import "SDVersion.h"

//setup for quickblox start
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Quickblox/Quickblox.h>
#import <QuickbloxWebRTC/QuickbloxWebRTC.h>
#import "QBUpdateUserParameters+CustomData.h"
#import "QBChatMessage+TextEncoding.h"
#import "QMProtocols.h"
#import <QMServices.h>
#import <Quickblox/QBCBlob.h>
#import <Quickblox/QBResponse.h>
#import <Quickblox/QBGeneralResponsePage.h>
#import <Quickblox/QBCBlob.h>

/*QMContentService*/
typedef void(^QMContentProgressBlock)(float progress);
typedef void(^QMCFileUploadResponseBlock)(QBResponse *response, QBCBlob *blob);
typedef void(^QMCFileDownloadResponseBlock)(QBResponse *response, NSData *fileData);
typedef void(^QBUUserPagedResponseBlock)(QBResponse *response, QBGeneralResponsePage *page, NSArray *users);

//setup for quickblox end


#endif
