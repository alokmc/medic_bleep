//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#ifndef ApplicationSpecificConstants_h
#define ApplicationSpecificConstants_h

/**
 Constants:-
 
 This header file holds all configurable constants specific  to this application.
 
 */

/**
 *
 *
 *   some macros to help calling common methods easily
 *
 *
 */
#define RETURN_IF_NO_INTERNET_AVAILABLE_WITH_USER_WARNING if (![CommonFunctions getStatusForNetworkConnectionAndShowUnavailabilityMessage:YES]) return;
#define RETURN_IF_NO_INTERNET_AVAILABLE                   if (![CommonFunctions getStatusForNetworkConnectionAndShowUnavailabilityMessage:NO]) return;
#define IS_INTERNET_AVAILABLE_WITH_USER_WARNING           [CommonFunctions getStatusForNetworkConnectionAndShowUnavailabilityMessage:YES]
#define IS_INTERNET_AVAILABLE                             [CommonFunctions getStatusForNetworkConnectionAndShowUnavailabilityMessage:NO]
#define SHOW_SERVER_NOT_RESPONDING_MESSAGE                [CommonFunctions showNotificationInViewController:self withTitle:nil withMessage:@"Server not responding .Please try again after some time." withType:TSMessageNotificationTypeError withDuration:MIN_DUR];
#define CONTINUE_IF_MAIN_THREAD if ([NSThread isMainThread] == NO) { NSAssert(FALSE, @"Not called from main thread"); }
#define FUNCTIONALLITY_PENDING_MESSAGE  [CommonFunctions showNotificationInViewController:APPDELEGATE.window.rootViewController withTitle:nil withMessage:@"We are still developing this functionallity ,please ignore it." withType:TSMessageNotificationTypeMessage withDuration:MIN_DUR];
#define IMG(x) [CommonFunctions getDeviceSpecificImageNameForName: x]
#define COMMON_VIEW_CONTROLLER_METHODS \
- (id)initWithNibName: (NSString *)nibNameOrNil bundle: (NSBundle *)nibBundleOrNil { \
self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];            \
if (self) {                                                                       \
}                                                                             \
return self;                                                                  \
}                                                                                 \
- (id)init {                                                                      \
self = [super init];                                                          \
if (self) {                                                                   \
}                                                                             \
return self;                                                                  \
}                                                                                 \
- (void)dealloc { \
[NSObject cancelPreviousPerformRequestsWithTarget:self]; \
[[NSNotificationCenter defaultCenter]removeObserver:self]; \
NSLog(@"%@ deallocated", [[self class]description]); \
[[NSUserDefaults standardUserDefaults]removeObjectForKey:[[self class]description]]; \
} \
- (void)didReceiveMemoryWarning {                                                 \
[super didReceiveMemoryWarning];                                              \
} \
- (void)viewDidDisappear:(BOOL)animated { \
CHECK_IF_VIEW_CONTROLLER_DEALLOCATED_WHEN_POPPED \
[super viewDidDisappear: animated]; \
}
#define WINDOW_OBJECT ((UIWindow *)[[[UIApplication sharedApplication].windows sortedArrayUsingComparator: ^NSComparisonResult (UIWindow *win1, UIWindow *win2) { return win1.windowLevel - win2.windowLevel; }]lastObject])
#define LOG_VIEW_CONTROLLER_LOADING NSLog(@"%@ loaded", [[self class]description]);
#define LOG_VIEW_CONTROLLER_APPEARING NSLog(@"%@ appears", [[self class]description]);
#define  ACF  [AppCommonFunctions sharedInstance]
#define  DBM  [DatabaseManager sharedDatabaseManager]
#define SHOW_EXCEPTION_MESSAGE(x) \
[CommonFunctions showNotificationInViewController: APPDELEGATE.window.rootViewController withTitle: nil withMessage: x withType: TSMessageNotificationTypeError withDuration: MIN_DUR];

#define NOTIFY_TO_UPDATE_LAYOUT_CUSTOM [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_NEED_LAYOUT_UPDATE_CUSTOM object:nil];
#define HIDE_TABBAR [[ACF tabBar]setTabBarHidden:YES animated:NO];
#define SHOW_TABBAR [[ACF tabBar]setTabBarHidden:NO animated:NO];
#define SHOW_COMING_SOON_MESSAGE_VIEW_CONTROLLER [ACF addComingSoonMessageOnView:[self view]];
#define PUSH_NOTIFICATION_TOKEN [[NSUserDefaults standardUserDefaults]objectForKey:PUSH_NOTIFICATION_DEVICE_TOKEN]
#define PATH_BY_APPENDING_DOCUMENT_DIRECTORY(x) [NSString stringWithFormat: @"%@/%@", [AKSMethods documentsDirectory], x]

/**
 *
 *
 *   some macros to enable/disable some functionalities
 *
 *
 */
#define PRINT_WEB_SERVICES_DETAILS 1
#define PRINT_MISSING_PARAMETER_DETAILS 1


/**
 *
 *
 *   some macros to simply hold some configurable constant values
 *
 *
 */

#define PUSH_NOTIFICATION_DEVICE_TOKEN @"deviceToken"
#define FAKE_PUSH_NOTIFICATION_DEVICE_TOKEN @"THIS IS FAKE PUSH NOTIFICATION DEVICE TOKEN"
#define MIN_DUR 3
#define TIME_DELAY_IN_FREQUENTLY_SAVING_CHANGES 1


/**
 *
 *
 *   some macros to simply hold user alert messages
 *
 *
 */
/*
 Message titles AND texts for all alerts in the application
 */

#define MINIMUM_LENGTH_LIMIT_USERNAME 1
#define MAXIMUM_LENGTH_LIMIT_USERNAME 20

#define MINIMUM_LENGTH_LIMIT_FIRST_NAME 0
#define MAXIMUM_LENGTH_LIMIT_FIRST_NAME 128

#define MINIMUM_LENGTH_LIMIT_LAST_NAME 0
#define MAXIMUM_LENGTH_LIMIT_LAST_NAME 128

#define MINIMUM_LENGTH_LIMIT_PASSWORD 6
#define MAXIMUM_LENGTH_LIMIT_PASSWORD 20

#define MINIMUM_LENGTH_LIMIT_MOBILE_NUMBER 7
#define MAXIMUM_LENGTH_LIMIT_MOBILE_NUMBER 14

#define MINIMUM_LENGTH_LIMIT_STREET 2
#define MAXIMUM_LENGTH_LIMIT_STREET 128

#define MINIMUM_LENGTH_LIMIT_CITY 2
#define MAXIMUM_LENGTH_LIMIT_CITY 32

#define MINIMUM_LENGTH_LIMIT_ZIP_CODE 4
#define MAXIMUM_LENGTH_LIMIT_ZIP_CODE 6

#define MINIMUM_LENGTH_LIMIT_COUNTRY 4
#define MAXIMUM_LENGTH_LIMIT_COUNTRY 24

#define MINIMUM_LENGTH_LIMIT_EMOTION_TEXT 1
#define MAXIMUM_LENGTH_LIMIT_EMOTION_TEXT 24

#define MINIMUM_LENGTH_LIMIT_STATUS_TEXT 1
#define MAXIMUM_LENGTH_LIMIT_STATUS_TEXT 140

#define MAXIMUM_LENGTH_LIMIT_VERIFICATION_NUMBER 4

#define MAXIMUM_LENGTH_LIMIT_EMAIL 64


/**
 *
 *
 *   some macros to represent notification names
 *
 *
 */
#define NOTIFICATION_NEED_LAYOUT_UPDATE_CUSTOM      @"NOTIFICATION_NEED_LAYOUT_UPDATE_CUSTOM"

//setup for quickblox start

#define QM_TEST 0

#define QM_AUDIO_VIDEO_ENABLED 1

#define DELETING_DIALOGS_ENABLED 0

#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
#define $(...)  [NSSet setWithObjects:__VA_ARGS__, nil]

#define CHECK_OVERRIDE()\
@throw\
[NSException exceptionWithName:NSInternalInconsistencyException \
reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]\
userInfo:nil]


/*ChatDialogs constants*/
static const NSUInteger kQMDialogsPageLimit = 10;

//******************** CoreData ********************

static NSString *const kChatCacheNameKey                    = @"q-municate";
static NSString *const kContactListCacheNameKey             = @"q-municate-contacts";
static NSString *const kUsersCacheNameKey                   = @"qb-users-cache";

//******************** Segue Identifiers ********************
static NSString *const kTabBarSegueIdnetifier               = @"TabBarSegue";
static NSString *const kSplashSegueIdentifier               = @"SplashSegue";
static NSString *const kLogInSegueSegueIdentifier           = @"LogInSegue";
static NSString *const kDetailsSegueIdentifier              = @"DetailsSegue";
static NSString *const kVideoCallSegueIdentifier            = @"VideoCallSegue";
static NSString *const kAudioCallSegueIdentifier            = @"AudioCallSegue";
static NSString *const kGoToDuringAudioCallSegueIdentifier  = @"goToDuringAudioCallSegueIdentifier";
static NSString *const kGoToDuringVideoCallSegueIdentifier  = @"goToDuringVideoCallSegueIdentifier";
static NSString *const kChatViewSegueIdentifier             = @"ChatViewSegue";
static NSString *const kIncomingCallIdentifier              = @"IncomingCallIdentifier";
static NSString *const kProfileSegueIdentifier              = @"ProfileSegue";
static NSString *const kCreateNewChatSegueIdentifier        = @"CreateNewChatSegue";
static NSString *const kGroupDetailsSegueIdentifier         = @"GroupDetailsSegue";
static NSString *const kQMAddMembersToGroupControllerSegue  = @"QMAddMembersToGroupControllerSegue";
static NSString *const kSettingsCellBundleVersion           = @"CFBundleVersion";

//******************** USER DEFAULTS KEYS ********************
static NSString *const kMailSubjectString                   = @"Medic Bleep";
static NSString *const kMailBodyString                      = @"<a href='http://quickblox.com/'>Join us in Medic Bleep!</a>";

//******************** PUSH NOTIFICATIONS ********************
static NSString *const kPushNotificationDialogIDKey         = @"dialog_id";

//***************** GROUP CHAT NOTIFICATIONS *****************
static NSString *const kDialogsUpdateNotificationMessage    = @"Notification message";

//setup for quickblox end


#endif
