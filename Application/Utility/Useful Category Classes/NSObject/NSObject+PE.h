//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSObject (PE)

- (BOOL)isNull:(NSObject *)object;
- (BOOL)isNotNull:(NSObject *)object;

@end
