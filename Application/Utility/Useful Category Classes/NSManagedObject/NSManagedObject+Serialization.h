//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//
#import <CoreData/CoreData.h>

@interface NSManagedObject (Serialization)

- (NSMutableDictionary*) toDictionary;

@end