//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//
#import <UIKit/UIKit.h>


@interface UIView (viewRecursion)

- (NSMutableArray *)allSubViews;
- (void)showActivityIndicator;
- (void)showActivityIndicatorWithStyle:(UIActivityIndicatorViewStyle)style;
- (void)hideActivityIndicator;

@end
