//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#import "UIView+RoundedCorners.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (RoundedCorners)

- (void)setRoundedCornersWithRadius:(CGFloat)radius borderColor:(UIColor *)color withBorderWidth:(float)width {
    [[self layer]setBorderColor:color.CGColor];
    [[self layer]setBorderWidth:width];
    [[self layer]setMasksToBounds:YES];
    [[self layer]setCornerRadius:radius];
}

- (void)makeCircularWithBorderColor:(UIColor *)color withBorderWidth:(float)width {
    [[self layer]setBorderColor:color.CGColor];
    [[self layer]setBorderWidth:width];
    [[self layer]setMasksToBounds:YES];
    [[self layer]setCornerRadius:[self size].width / 2];
}

@end
