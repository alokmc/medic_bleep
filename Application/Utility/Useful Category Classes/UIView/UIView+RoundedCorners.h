//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UIView (RoundedCorners)

- (void)setRoundedCornersWithRadius:(CGFloat)radius borderColor:(UIColor *)color withBorderWidth:(float)width;
- (void)makeCircularWithBorderColor:(UIColor *)color withBorderWidth:(float)width;

@end
