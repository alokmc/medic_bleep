//
//  Created by Alok Singh on 15/01/16.
//  Copyright (c) 2015 CoreAura. All rights reserved.
//

#ifndef AllCategories_h
#define AllCategories_h

#import "NSDate+Helper.h"
#import "NSDate+Utilities.h"
#import "NSObject+PE.h"
#import "NSString+HTML.h"
#import "NSString+Common.h"
#import "NSUserDefaults+Convenience.h"
#import "UIView+FrameHelper.h"
#import "NSString+Customised.h"
#import "NSDate+Customised HumanInterval.h"
#import "UIView+All Subview.h"
#import "UIView+RoundedCorners.h"
#import "NSString+Random.h"
#import "NSManagedObject+Serialization.h"

#endif
